python-pynzb (0.1.0-4) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Carl Suster ]
  * d/control: Remove myself from Uploaders

 -- Ondřej Nový <onovy@debian.org>  Tue, 13 Feb 2018 10:04:06 +0100

python-pynzb (0.1.0-3) unstable; urgency=medium

  * Add myself to uploaders.
  * Switch to pybuild and dh-python.
  * Bump d/compat to 10 and update version of B-D on debhelper.
  * Bump standards-version to 3.9.8 (no changes).
  * d/copyright: add myself and fix license short names:
    - "public domain" -> public-domain,
    - BSD -> BSD-3-clause.
  * Change Vcs to DPMT git repository and use https.
  * Change Homepage to Github.
  * Build the Python 3 module.
    - replace B-D: python{,3}-setuptools and python{,3}-all
  * Drop the Python 2 module (no rdeps):
  * Run the test suite with pytest:
    - cleanup the produced .cache/ in d/clean,
    - add B-D on python3-pytest.
  * 0001-set-message_id-properly-in-expat-parser.patch: fix an upstream code
    typo. This change allows the tests to pass for Python 2.
  * 0002-enable-use_2to3-in-setup.py.patch: enable 2to3 invocation in setup.py.
  * Move lxml to Suggests rather than Depends since there are fallbacks using
    standard library XML parsers.
  * Build-Depend on lxml in order to run the test for the implementation of the
    NZB parser using lxml (LXMLNZBParser).
  * 0003-give-lxml-etree-BytesIO-in-Python-3.patch: make the code Python 3
    compatible by decoding strings -> bytes as UTF-8 and substituting BytesIO
    for StringIO. This only affects the LXMLNZPParser.
  * Fix watch file and declare version 4 format.
  * Cleanup .egg-info files in d/clean and d/source/options.

 -- Carl Suster <carl@contraflo.ws>  Thu, 12 Jan 2017 12:30:23 +1100

python-pynzb (0.1.0-2) unstable; urgency=low

  * use https:// URL for pypi in debian/watch
  * clarify description (Closes: #749218)

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 23 Oct 2014 12:31:55 -0400

python-pynzb (0.1.0-1) unstable; urgency=low

  * source package automatically created by stdeb 0.6.0+git
  * Initial release (Closes: #723170)

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 16 Sep 2013 22:20:44 -0400
